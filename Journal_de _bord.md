## Objectif:

Repérer les sujets et les expressions qui ont fait l’actualité dans les publications

# Cours en 4 partie:

- Obtenir des données (GitLab, extraction de texte)
- Enrichir les données (Analyse automatque, extraction de patrons
- Analyse (Topic modeling, analyse dans le temps)
- Visualisation (mise en forme des sortes de modèles, export pour le rapport web)

# Deux types de rendu:

Rapport final et rendu gitlab chaque semaine.
